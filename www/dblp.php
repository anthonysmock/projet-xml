<!DOCTYPE html>
<html>
	<head>
		<title>showtest</title>
		<link rel="stylesheet" type="text/css" href="/basic.css"/>
	</head>
	
	<body>

		<?php		
			$dom = new DOMDocument();
			$xsl = new DOMDocument();
			$proc = new XSLTProcessor;
			$dom->validateOnParse = true;

			$dom->load('data/out.xml');
			$xsl->load('data/dblp.xsl');
			$proc->importStyleSheet($xsl);

			$time_start = microtime(true);
			
			echo $proc->transformToXML($dom);
						
			$time_end = microtime(true);
			$time = $time_end - $time_start;
			echo $time.'<br/>';
		?>

	</body>
</html>
