<?php
session_start();
if(!isset($_SESSION['file']))
{
	header('Location: /index.php');
	exit();
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta  http-equiv="Content-Type" content="text/html">
	<link rel="stylesheet" type="text/css" href="/basic.css"/>
    <title>Search Publications</title>
  </head>
  <body>
    <h3>Search  Dblp</h3>
	<form method="get" action="recherche.php">
		Title:<input  type="text" name="saisie"/><br/>
		Author:<input  type="text" name="author"/><br/>
		<input  type="submit" name="aut" value="Search"/>
      		<p>Type : 
      			<select name="type">
				<!--<option value="">Tous les elements</option>-->
				<option value="article">Article</option>
				<option value="book">Books</option>
				<option value="phdthesis">phd thesis</option>
				<option value="mastersthesis">masters thesis</option>
				<option value="www">web site</option>
				<option value="inproceedings">inproceedings</option>
				<option value="proceedings">proceedings</option>
				<!--<option value="data">Data</option>-->
   			</select>
  		</p>
	</form>

<?php
  if(isset($_GET['saisie'])){ //defini suite a clique formulaire ou lien filtre
   $type = $_GET['type'];
   $saisie=$_GET['saisie'];
   echo "<p>saisie: $saisie</p>";
   affichageResultat($saisie, $type);
  }
  else{
	  echo  "<p>Please enter a search query</p>";
  }
?>


<?php
function affichageResultat($name , $type) {
	$time_start = microtime(true);
	
	$dom = new DOMDocument;
	$dom->validateOnParse = true;
	$dom->substituteEntities = true;
	$dom->load($_SESSION['file']);
	
	$xsl = new DOMDocument;
	$xsl->load('data/dblp.xsl');
	
	$proc = new XSLTProcessor();
	$proc->importStyleSheet($xsl);
	
	$xpath = new DOMXPath($dom);

	set_time_limit(420);
	
	$sstype="";
	if(!isset($_GET['sstype'])) $sstype = "title";
	else $sstype = $_GET['sstype'];
	
	if(isset($_GET['author']))
		$query = "/dblp/".$type."[contains(".$sstype."/text(), '".$name."')][contains(".$sstype."/text(), '".$_GET['author']."')]";
	else
		$query = "/dblp/".$type."[contains(".$sstype."/text(), '".$name."')]";
	$ress =  $xpath->query($query);

	$domres = new DOMDocument();
	foreach($ress as $res)
	{
		$domres->appendChild($domres->importNode($res, true));
	}
	
	$html = '<div class="res" float:left;width:60%; border: 1px solid black>'.$proc->transformToXML($domres).'</div>';
	/*$query2 = "//author[not(. = preceding::author)]";
	$ress2 =  $xpath->query($query2);*/
	
	$time_end = microtime(true);
	$time = $time_end - $time_start;
	echo 'time : '.$time.'<br/>';
	echo $html;
	
	/*echo '<div style="float:left;width:30%; border: 1px solid black">Filtres de recherche"';
	foreach($ress2 as $res)
	{
		echo "<li><div><span><a href=\"recherche.php?saisie=".$name."&author=".$res->nodeValue."&type=article&sstype=author\"> ".$res->nodeValue."</a></span></div></li>";
	}
	echo "</div>";*/
	
	
}
	
	//Affichage des 3 listes dans les 3 divs auteurs publication et venues
	function affichageList($list)
	{
		echo "<ul>";
		foreach ($list as $k => $v) {
				echo "<li><div>";
				echo "<button onclick=\"location.href='$v'\">View</button>";
				echo "<span>".$k."</span></li>";
		}
		echo "</ul>";
		echo"</div>";
	}
	

	//Affichage des filtres de recherche
	function affichageFiltre($list, $count,$name)
	{
    
    $cpt = array();
    $listFiltres = array();

    foreach ($list as $k => $v) {
      $cpt[$k] = 0;
      for ($i = 0; $i < $count; $i++) {
        $booltrouve = false;
         for ($j = $i +1 ; $j < $count; $j++) {
          if($v[$i] === $v[$j] ){
            $booltrouve = true;
          }
        } 
        if($booltrouve === false){
          $listFiltres[$k][$cpt[$k]] =  $v[$i];
          $cpt[$k] ++;
        }
      }
    }

		foreach ($listFiltres as $k => $v) {
			echo "<ul><div><span>".$k."</span></div>";
			for ($i = 0; $i < $cpt[$k]; $i++) {
				echo "<li><div><span><a href=\"recherche.php?saisie=".$name."&".$k."=".$v[$i]."&type=\"> ".$v[$i]."</a></span></div></li>";
			}
			echo "</ul>";
		}
		echo"</div>";
	}
?>

  </body>
</html>
