<html>
  <head>
    <meta  http-equiv="Content-Type" content="text/html;  charset=iso-8859-1">
    <title>Insert Publications</title>
  </head>

  <body>
    <h3>Insert  Dblp</h3>

		<!-- ARTICLE PART-->
		<div>
			<h4>Add Article</h4>
			<form method="post" action="addArticle.php">
				mdate : <input type="text" name="mdate"><br>
				Review of parution : <input type="text" name="key"><br>
				Author(s) : <input type="text" name="author"><br>
				<i>If there are multiple authors, separate the name by a ";"<br></i>
				Title : <input type="text" name="title"><br>
				Pages : <input type="text" name="pages"><br>
				Year : <input type="text" name="year"><br>
				Volume : <input type="text" name="volume"><br>
				Journal : <input type="text" name="journal"><br>
				Number : <input type="text" name="number"><br>
				URL : <input type="text" name="url"><br>
				EE : <input type="text" name="ee"><br>
				<input type="submit" value="Add New Article">
			</form>
		</div>

    <!-- BOOK PART -->
    <div>
			<h4>Add Book</h4>
			<form method="get" action="addBook.php">
		    mdate : <input type="text" name="mdate"><br>
				Review of parution : <input type="text" name="key"><br>
				Editor(s) : <input type="text" name="editor"><br>
				<i>If there are multiple editor, separate the name by a ";"<br></i>
				Title : <input type="text" name="title"><br>
				Publisher : <input type="text" name="publisher"><br>
				Year : <input type="text" name="year"><br>
				ISBN : <input type="text" name="isbn"><br>
				URL : <input type="text" name="url"><br>
				<input type="submit" value="Add New Book">
			</form>
		</div>

    <!-- PHD PART -->
		<div>
	    <h4>Add Phd Thesis</h4>
			<form method="get" action="addPhdThesis.php">
		    mdate : <input type="text" name="mdate"><br>
				Review of parution : <input type="text" name="key"><br>
				Author : <input type="text" name="editor"><br>
				Title : <input type="text" name="title"><br>
				Pages : <input type="text" name="pages"><br>
				School : <input type="text" name="school"><br>
				Year : <input type="text" name="year"><br>
				EE : <input type="text" name="ee"><br>
				<input type="submit" value="Add New PHD Thesis">
			</form>			
		</div>

    <!-- MASTER PART -->
		<div>
			<h4>Add Master Thesis</h4>
			<form method="get" action="addMasterThesis.php">
		    mdate : <input type="text" name="mdate"><br>
				Review of parution : <input type="text" name="key"><br>
				Author : <input type="text" name="editor"><br>
				Title : <input type="text" name="title"><br>
				Pages : <input type="text" name="pages"><br>
				School : <input type="text" name="school"><br>
				Year : <input type="text" name="year"><br>
				EE : <input type="text" name="ee"><br>
				<input type="submit" value="Add New Master Thesis"> 
			</form>
		</div>

</body>