<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- xml to html -->
	
	<xsl:template match='/'>
		<xsl:apply-templates select="*">
			<xsl:sort select="year" order="descending"/>
		</xsl:apply-templates>
	</xsl:template>
	
	<xsl:template match='article'>
		<div class='article pub'>
			<xsl:call-template name="fields"/>
		</div>
	</xsl:template>
	
	<xsl:template match='inproceedings'>
		<div class='inproceedings pub'>
			<xsl:call-template name="fields"/>
		</div>
	</xsl:template>
	
	<xsl:template match='proceedings'>
		<div class='proceedings pub'>
			<xsl:call-template name="fields"/>
		</div>
	</xsl:template>
	
	<xsl:template match='book'>
		<div class='book pub'>
			<xsl:call-template name="fields"/>
		</div>
	</xsl:template>
	
	<xsl:template match='incollection'>
		<div class='incollection pub'>
			<xsl:call-template name="fields"/>
		</div>
	</xsl:template>
	
	<xsl:template match='phdthesis'>
		<div class='phdthesis pub'>
			<xsl:call-template name="fields"/>
		</div>
	</xsl:template>
	
	<xsl:template match='mastersthesis'>
		<div class='mastersthesis pub'>
			<xsl:call-template name="fields"/>
		</div>
	</xsl:template>
	
	<xsl:template match='www'>
		<div class='www'>
			<h4><xsl:apply-templates select='url'/></h4>
			<xsl:for-each select='author'>
				<xsl:apply-templates select='.'/>
				<xsl:if test='position() != last()'>, </xsl:if>
				<xsl:if test='position() = last()'>.</xsl:if>
			</xsl:for-each>
		</div>
	</xsl:template>
	
	<xsl:template name='fields'>
		<xsl:for-each select='author'>
			<xsl:apply-templates select='.'/>
			<xsl:if test='position() != last()'>, </xsl:if>
			<xsl:if test='position() = last()'>:</xsl:if>
		</xsl:for-each>
		<xsl:if test="title"><h3><xsl:apply-templates select='title'/></h3></xsl:if>
		<xsl:if test="booktitle"><xsl:apply-templates select='booktitle'/></xsl:if>
		<xsl:if test="journal"> <xsl:apply-templates select='journal'/></xsl:if>
		<xsl:if test="number">(<xsl:apply-templates select='number'/>)</xsl:if>
		<xsl:if test="pages">: <xsl:apply-templates select='pages'/></xsl:if>
		<xsl:if test="year"> (<xsl:apply-templates select='year'/>)</xsl:if>
		<!--<xsl:apply-templates select='editor'/>
		<xsl:apply-templates select='address'/>
		<xsl:apply-templates select='month'/>
		<xsl:apply-templates select='url'/>
		<xsl:apply-templates select='ee'/>
		<xsl:apply-templates select='cdrom'/>
		<xsl:apply-templates select='cite'/>
		<xsl:apply-templates select='publisher'/>
		<xsl:apply-templates select='note'/>
		<xsl:apply-templates select='crossref'/>
		<xsl:apply-templates select='isbn'/>
		<xsl:apply-templates select='series'/>
		<xsl:apply-templates select='school'/>
		<xsl:apply-templates select='chapter'/>
		<xsl:apply-templates select='publnr'/>-->
		
	</xsl:template>
	
	<xsl:template match='title'>
		<span class='title'><xsl:value-of select='text()'/></span>
	</xsl:template>

	<xsl:template match='author'>
		<span class='author'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='editor'>
		<span class='editor'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='booktitle'>
		<span class='booktitle'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='year'>
		<span class='year'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='address'>
		<span class='address'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='journal'>
		<span class='journal'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='volume'>
		<span class='volume'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='number'>
		<span class='number'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='month'>
		<span class='month'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='url'>
		<span class='url'><xsl:value-of select='text()'/></span>
	</xsl:template>

	<xsl:template match='ee'>
		<span class='ee'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='cdrom'>
		<span class='cdrom'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='cite'>
		<span class='cite'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='publisher'>
		<span class='publisher'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='note'>
		<span class='note'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='crossref'>
		<span class='crossref'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='isbn'>
		<span class='isbn'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='series'>
		<span class='series'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='school'>
		<span class='school'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='chapter'>
		<span class='chapter'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
	<xsl:template match='publnr'>
		<span class='publnr'><xsl:value-of select='text()'/></span>
	</xsl:template>
	
</xsl:stylesheet>