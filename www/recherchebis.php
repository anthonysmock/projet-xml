
<html>
  <head>
    <meta  http-equiv="Content-Type" content="text/html;  charset=iso-8859-1">
    <title>Search Publications</title>
  </head>
  <p><body>
    <h3>Search  Dblp</h3>
	<form method="get" action="recherchebis.php">
	      <input  type="text" name="saisie">
      <input  type="submit" name="aut" value="Search">
	        <p>Type : 
      <select name="type">
        <option value="">Tous les elements</option>
        <option value="article">Article</option>
        <option value="book">Books</option>
        <option value="phdthesis">phd thesis</option>
        <option value="mastersthesis">masters thesis</option>
        <option value="www">web site</option>
        <option value="data">Data</option>
        <option value="inproceedings">inproceedings</option>
        <option value="proceedings">proceedings</option>
        <option value="data">Data</option>
      </select>
      </p>
	</form>
  </body>
</html>



<?php
   error_reporting(0); //cache les warning
  if(isset($_GET['saisie'])){ //defini suite a clique formulaire ou lien filtre
   $type = $_GET['type'];
   $saisie=$_GET['saisie'];
   echo "<p>saisie: $saisie</p>";
   affichageResultat($saisie, $type);
  }
  else{
  echo  "<p>Please enter a search query</p>";
  }
?>


<?php
    function affichageResultat($name , $type) {
        $dom    = new DOMDocument();
        $xpath  = new DOMXPath($dom);
        $reader = new XMLReader();
		
		$listAuteur = array();
		$listPublication = array();
		$listVenue = array();
		$listFiltres = array();

        /*LIBXML_PARSEHUGE permet de retirer toutes les limites, comme une durée de 120 secondes
          pour le traitement. Laisse la liberté de mettre un chiffre bien supérieur à 500
          en $count (voir ci-dessous)*/
        $reader->open('data/out.xml' ,LIBXML_PARSEHUGE);

        //Il faut charger le DTD, ou le parser ne connaîtra pas les définitions &xxx;
        $reader->setParserProperty(XMLReader::LOADDTD,  TRUE);
        //$reader->setParserProperty(XMLReader::VALIDATE, TRUE);

        $count = 0;

        /*Avec un XMLReader, lit article par article, mais fait tous les traitements avec DOM.
          Permet de traiter le fichier avec DOM mais sans avoir à le charger entièrement.
          Limite arbitraire à 500 élèments, pour l'instant*/
        while ($reader->read()) {
           
            //TODO: regarder à la DTD et gérer les autres éléments autres qu'article mais au même niveau
            if ($reader->nodeType == XMLReader::ELEMENT && ( $reader->name == $type || ($type == '' && ($reader->name == 'article' || $reader->name == 'book' || $reader->name == 'phdthesis' || $reader->name == 'mastersthesis' || $reader->name == 'www' )))) {
                
                $count++;
                $node = $dom->importNode($reader->expand(), true);
                //Ne fait que récupérer la représentation textuelle, pour l'instant.
                $result = $xpath->evaluate(
                    'string()',
                    $node
                );
				
				//ciblage des noeuds auteur title et journal
				$authors = $node->getElementsByTagName( "author" );          
				$author = $authors->item(0)->nodeValue;
				$publications = $node->getElementsByTagName( "title" );          
				$publication = $publications->item(0)->nodeValue;
				$venues = $node->getElementsByTagName( "journal" );          
				$venue = $venues->item(0)->nodeValue;
				
				$urls = $node->getElementsByTagName( "ee" );          
				$url = $urls->item(0)->nodeValue;
				
				//contient la chaine de caractere saisie on laffiche en ajoutant a la liste. non sensible a la casse
				if (stripos($author, $name) !== false) 
				{
					//filtre aussi sur le nom de lauteur ou on a cliqué
					if((!isset($_GET['author'])) || (stripos($author, $_GET['author']) !== false)) 
					{
						$listAuteur[$author] = $url;
						//filtre rattache a cette recherche
						$listFiltres["author"][$author] = "";$listFiltres["author"][$author] = $author;
						$listFiltres["publication"][$publication] = ""; $listFiltres["publication"][$publication] = $publication;
					   $listFiltres["venue"][$venue] = ""; $listFiltres["venue"][$venue] = $venue;
				}
			   if (stripos($publication, $name) !== false) 
			   {
				   //filtre aussi sur le nom de la publication ou on a cliqué
				   if((!isset($_GET['publication'])) || (stripos($publication, $_GET['publication']) !== false)) 
				   {
					   $listPublication[$publication] = $url;
						//filtre rattache a cette recherche
						$listFiltres["author"][$author] = "";$listFiltres["author"][$author] = $author;
						$listFiltres["publication"][$publication] = ""; $listFiltres["publication"][$publication] = $publication;
					   $listFiltres["venue"][$venue] = ""; $listFiltres["venue"][$venue] = $venue;
				   }
			   }
			   if (stripos($venue, $name) !== false) 
			   {
				   //filtre aussi sur le nom de la venue ou on a cliqué
				   if((!isset($_GET['venue'])) || (stripos($venue, $_GET['venue']) !== false)) 
				   {
					   $listVenue[$venue] = $url;
						//filtre rattache a cette recherche
						$listFiltres["author"][$author] = "";$listFiltres["author"][$author] = $author;
						$listFiltres["publication"][$publication] = ""; $listFiltres["publication"][$publication] = $publication;
					   $listFiltres["venue"][$venue] = ""; $listFiltres["venue"][$venue] = $venue;
				   }
			   }
            }
			}
        }
        $reader->close();
		
		echo "<div id=\"containter\">";
		echo "<div style=\"float:left;width:20%;\" id=\"1\">Auteurs (".count($listAuteur).")";
		affichageList($listAuteur);
		echo "<div style=\"float:left;width:30%;\" id=\"2\">Publications (".count($listPublication).")";
		affichageList($listPublication);
		echo"<div style=\"float:left;width:20%;\" id=\"3\">Venues (".count($listVenue).")";
		affichageList($listVenue);
		echo"<div style=\"float:left;width:30%;\" id=\"4\">Filtres de recherche";
		affichageFiltre($listFiltres,$count,$name);
		echo"</div>";
    }
	
	//Affichage des 3 listes dans les 3 divs auteurs publication et venues
	function affichageList($list)
	{
		echo "<ul>";
		foreach ($list as $k => $v) {
				echo "<li><div>";
				echo "<button onclick=\"location.href='$v'\">View</button>";
				echo "<span>".$k."</span></li>";
		}
		echo "</ul>";
		echo"</div>";
	}
	

	//Affichage des filtres de recherche
	function affichageFiltre($listFiltres, $count,$name)
	{
		foreach ($listFiltres as $k => $v) {
			echo "<ul><div><span>".$k."</span></div>";
				foreach ($v as $kg => $vg) {
				echo "<li><div><span><a href=\"recherchebis.php?saisie=".$name."&".$k."=".$kg."&type=\"> ".$kg."</a></span></div></li>";
			}
			echo "</ul>";
		}
		echo"</div>";
	}
?>

