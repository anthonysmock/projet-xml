<?php
session_start();
if(isset($_POST['file']))
{
	$_SESSION['file'] = $_POST['file'];
	header('Location: /accueil.html');
	exit();
}
else if(isset($_SESSION['file']))
	echo 'Current file selected: '.$_SESSION['file'].'<br/><a href="/recherche.php">Query data</a>';
?>

<!DOCTYPE html>
<html>
	<head>
		<title>projet-xml</title>
	</head>
	
	<body>
		<form style="border-style: solid; padding: 50px 30px 50px 80px;" action="/index.php" method="post">
		<h3>Use an existing xml file:</h3>
			<select name="file">
				<?php
					foreach(glob('data/*.xml') as $file)
						echo '<option value="'.$file.'">'.end(explode("/", $file)).'</option>'
				?>
			</select>
			<input type="submit">
		</form>
		
		<form style="border-style: solid; padding: 50px 30px 50px 80px;" action="/xml/create.php" method="post">
			<h3>Create a new xml file by extracting elements in dblp.xml (you must have it in the data folder)</h3>
			File name (without ".xml"): <input type="text" name="fname"/><br/>
			Length: <input type="text" name="length"/><br/>
			<input type="radio" name="length_type" value="elements" checked>Number of elements<br/>
			<input type="radio" name="length_type" value="size">Size in Ko<br/>
			<strong>Create (it can take 10 minutes):</strong><br/>
			<input type="submit">
		</form>
	</body>
</html>
