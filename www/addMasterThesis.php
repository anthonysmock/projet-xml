<?php
  session_start();
  error_reporting(0); //cache les warning
  if(isset($_GET['mdate']) && isset($_GET['key']) && isset($_GET['author']) && isset($_GET['title'])
    && isset($_GET['pages']) && isset($_GET['school']) && isset($_GET['year']) && isset($_GET['ee']))
  { //defini suite a clique formulaire ou lien filtre
    $mdate = $_GET['mdate'];
    $key=$_GET['key'];
    $author=$_GET['author'];
    $title=$_GET['title'];
    $pages=$_GET['pages'];
    $school=$_GET['school'];
    $year=$_GET['year'];
    $ee=$_GET['ee'];

    addMasterThesis($mdate, $key, $author, $title, $pages, $school, $year, $ee);
  }
  else
  {
    echo  "<p>All fields must be filled</p>";
  }
?>

<?php
  function addMasterThesis()
  {    
    $doc = new DOMDocument();
    $doc->formatOutput = true;
    $doc->load($_SESSION['file']);

    // Creation d'un nouveau noeud
    $dblp = $doc->getElementsByTagName('dblp');

    $article = $doc->createElement("article");
    $dblp->appendChild($article);

    // Creation des attributs liés au noeud
    $mDateAttribut = $doc->createAttribute("mdate");
    $mDateAttribut->value = $mdate;
    $article->appendChild($mDateAttribut);

    $keyAttribut = $doc->createAttribute("key");
    $keyAttribut->value = $key;
    $article->appendChild($keyAttribut);

    $author = $doc->createElement( "author" );
    $author->appendChild($doc->createTextNode( $article['author']));

    $title = $doc->createElement( "title" );
    $title->appendChild($doc->createTextNode( $article['title']));

    $pages = $doc->createElement( "pages" );
    $pages->appendChild($doc->createTextNode( $article['pages']));

    $school = $doc->createElement( "school" );
    $school->appendChild($doc->createTextNode( $article['school']));

    $year = $doc->createElement( "year" );
    $year->appendChild($doc->createTextNode( $article['year']));

    $volume = $doc->createElement( "volume" );
    $volume->appendChild($doc->createTextNode( $article['volume']));

    $ee = $doc->createElement( "ee" );
    $ee->appendChild($doc->createTextNode( $article['ee']));
   
    file_put_contents($_SESSION['file'], $doc->saveXML());
  }
?>




