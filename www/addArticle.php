<?php
  session_start();
  if(isset($_POST['mdate']) && isset($_POST['key']) && isset($_POST['author']) && isset($_POST['title'])
    && isset($_POST['pages']) && isset($_POST['year']) && isset($_POST['volume']) && isset($_POST['journal']) && isset($_POST['number']) && isset($_POST['url']) && isset($_POST['ee']))
  { //defini suite a clique formulaire ou lien filtre
    $mdate = $_POST['mdate'];
    $key=$_POST['key'];
    $author=$_POST['author'];
    $title=$_POST['title'];
    $pages=$_POST['pages'];
    $year=$_POST['year'];
    $volume=$_POST['volume'];
    $journal=$_POST['journal'];
    $number=$_POST['number'];
    $url=$_POST['url'];
    $ee=$_POST['ee'];

    addArticle($mdate, $key, $author, $title, $pages, $year, $volume, $journal, $number, $url, $ee);
  }
  else
  {
    echo  "<p>All fields must be filled</p>";
  }
?>

<?php
  function addArticle($mdate, $key, $author, $title, $pages, $year, $volume, $journal, $number, $url, $ee)
  {    
    $doc = new DOMDocument();
    $doc->formatOutput = true;
	$doc->validateOnParse = true;

    $doc->load($_SESSION['file']);

    // Creation d'un nouveau noeud
    $dblp = $doc->GetElementsByTagName('dblp')->item(0);

    $article = $doc->createElement("article");
    $dblp->appendChild($article);

    // Creation des attributs liés au noeud
    $mDateAttribut = $doc->createAttribute("mdate");
    $mDateAttribut->value = $mdate;
    $article->appendChild($mDateAttribut);

    $keyAttribut = $doc->createAttribute("key");
    $keyAttribut->value = $key;
    $article->appendChild($keyAttribut);

    // Pour chaque auteur
    $authorTab = explode(";", $author);
    foreach ($authorTab as $author1) 
    {
      $node = $doc->createElement( "author" );
	  $node->appendChild($doc->createTextNode( $author1));
      $article->appendChild($node);
    }

    $title = $doc->createElement( "title" );
    //$title->appendChild($doc->createTextNode( $title));
	$article->appendChild($title);

    $pages = $doc->createElement( "pages" );
    $pages->appendChild($doc->createTextNode( $pages));
	$article->appendChild($pages);

    $year = $doc->createElement( "year" );
    $year->appendChild($doc->createTextNode( $year));
	$article->appendChild($year);

    $volume = $doc->createElement( "volume" );
    $volume->appendChild($doc->createTextNode( $volume));
	$article->appendChild($volume);

    $journal = $doc->createElement( "journal" );
    $journal->appendChild($doc->createTextNode( $journal));
	$article->appendChild($journal);

    $number = $doc->createElement( "number" );
    $number->appendChild($doc->createTextNode( $number));
	$article->appendChild($number);

    $url = $doc->createElement( "url" );
    $url->appendChild($doc->createTextNode( $url));
	$article->appendChild($url);

    $ee = $doc->createElement( "ee" );
    $ee->appendChild($doc->createTextNode( $ee));
	$article->appendChild($ee);
   
    file_put_contents($_SESSION['file'], $doc->saveXML());
  }
?>




