<?php
  session_start();
  error_reporting(0); //cache les warning
  if(isset($_GET['mdate']) && isset($_GET['key']) && isset($_GET['editor']) && isset($_GET['title'])
    && isset($_GET['publisher']) && isset($_GET['year']) && isset($_GET['isbn']) && isset($_GET['url']))  { //defini suite a clique formulaire ou lien filtre
    $mdate = $_GET['mdate'];
    $key=$_GET['key'];
    $editor=$_GET['editor'];
    $title=$_GET['title'];
    $publisher=$_GET['publisher'];
    $year=$_GET['year'];
    $isbn=$_GET['isbn'];
    $url=$_GET['url'];

    addBook($mdate, $key, $editor, $title, $publisher, $year, $isbn, $url);
  }
  else
  {
    echo  "<p>All fields must be filled</p>";
  }
?>

<?php
  function addBook()
  {    
    $doc = new DOMDocument();
    $doc->formatOutput = true;
    $doc->load($_SESSION['file']);

    // Creation d'un nouveau noeud
    $dblp = $doc->getElementsByTagName('dblp');

    $article = $doc->createElement("book");
    $dblp->appendChild($article);

    // Creation des attributs liés au noeud
    $mDateAttribut = $doc->createAttribute("mdate");
    $mDateAttribut->value = $mdate;
    $article->appendChild($mDateAttribut);

    $keyAttribut = $doc->createAttribute("key");
    $keyAttribut->value = $key;
    $article->appendChild($keyAttribut);

    // Pour chaque auteur
    $editorTab = explode(";", $editor);
    foreach ($editorTab as $editor) 
    {
      $editor = $doc->createElement( "editor" );
      $editor->appendChild($doc->createTextNode( $article['editor']));
    }

    $title = $doc->createElement( "title" );
    $title->appendChild($doc->createTextNode( $article['title']));

    $publisher = $doc->createElement( "publisher" );
    $publisher->appendChild($doc->createTextNode( $article['publisher']));

    $year = $doc->createElement( "year" );
    $year->appendChild($doc->createTextNode( $article['year']));

    $isbn = $doc->createElement( "isbn" );
    $isbn->appendChild($doc->createTextNode( $article['isbn']));
 
    $url = $doc->createElement( "url" );
    $url->appendChild($doc->createTextNode( $article['url']));
   
    file_put_contents($_SESSION['file'], $doc->saveXML());
  }
?>




