		<?php
		session_start();
			if($_POST['length_type'] == 'elements')
				$item_limit = intval($_POST['length']);
			else
				$item_limit = 2500 * intval($_POST['length']);
			$time_start = microtime(true);
		
			$nb_item = 5663163 - 1903295;
			
			$n_item['article'] = 1638406;
			$n_item['book'] = 13495;
			$n_item['inproceedings'] = 1976532;
			$n_item['proceedings'] = 33696;
			$n_item['incollection'] = 42600;
			$n_item['phdthesis'] = 55129;
			$n_item['mastersthesis'] = 10;
			//$n_item['www'] = 1903295;
			
			
			$r_item['article'] = $n_item['article'] / $nb_item;
			$r_item['book'] = $n_item['book'] / $nb_item;
			$r_item['inproceedings'] = $n_item['inproceedings'] / $nb_item;
			$r_item['proceedings'] =  $n_item['proceedings'] / $nb_item;
			$r_item['incollection'] = $n_item['incollection'] / $nb_item ;
			$r_item['phdthesis'] = $n_item['phdthesis'] / $nb_item;
			$r_item['mastersthesis'] = $n_item['mastersthesis'] / $nb_item;
			//$r_item['www'] = $n_item['www'] / $nb_item;
			
			$m_item['article'] = ceil($r_item['article'] * $item_limit);
			$m_item['book'] = ceil($r_item['book'] * $item_limit);
			$m_item['inproceedings'] = ceil($r_item['inproceedings'] * $item_limit);
			$m_item['proceedings'] =  ceil($r_item['proceedings'] * $item_limit);
			$m_item['incollection'] = ceil($r_item['incollection'] * $item_limit);
			$m_item['phdthesis'] = ceil($r_item['phdthesis'] * $item_limit);
			$m_item['mastersthesis'] = ceil($r_item['mastersthesis'] * $item_limit) + 1;
			//$m_item['www'] = ceil($r_item['www'] * $item_limit);
			
			/*$m_item['article'] = ceil ($m_item['article'] * 1.5);
			$m_item['www'] = ceil ($m_item['www'] * 0.5);*/
			
			$n_item_sum = $m_item['article'] + $m_item['book'] + $m_item['inproceedings'] + $m_item['proceedings'] +
					$m_item['incollection'] + $m_item['phdthesis'] + $m_item['mastersthesis'];
					
			$item_limit = ($item_limit < $n_item_sum) ? $item_limit : $n_item_sum;
					
			if($item_limit < 1)
				$item_limit = 1;
			
			$count = 0;
				
			$c_item['article'] = 0;
			$c_item['book'] = 0;
			$c_item['inproceedings'] = 0;
			$c_item['proceedings'] =  0;
			$c_item['incollection'] = 0;
			$c_item['phdthesis'] = 0;
			$c_item['mastersthesis'] = 0;
			//$c_item['www'] = 0;
			
			$imp = new DOMImplementation;
			$output_dom = $imp->createDocument(null, 'dblp', 
				$imp->createDocumentType('dblp', null, 'dblp.dtd'));
				
			$output_dom->encoding ='UTF-8';
			$output_dom->formatOutput = true;
			
			$dom = new DOMDocument();
			$reader = new XMLReader();
			
			/*LIBXML_PARSEHUGE permet de retirer toutes les limites, comme une durée de 120 secondes
			  pour le traitement. Laisse la liberté de mettre un chiffre bien supérieur à 500
			  en $count (voir ci-dessous)*/
			$reader->open('../data/dblp.xml' ,LIBXML_PARSEHUGE);
			
			//Il faut charger le DTD, ou le parser ne connaîtra pas les définitions &xxx;
			$reader->setParserProperty(XMLReader::LOADDTD,  TRUE); 
			
			//Place juste avant le premier article
			$reader->read();
			$reader->read();
			$reader->read();
			//*************************************
			
			$dblp = $output_dom->documentElement;

			while ($reader->next() && $count < $item_limit) {
				
				if ($reader->nodeType == XMLReader::ELEMENT && isset($m_item[$reader->name]) && $c_item[$reader->name] < $m_item[$reader->name]) {
					
					$node = $output_dom->importNode($reader->expand(), true);
					$dblp->appendChild($node);
					$c_item[$reader->name]++;
					$count++;
				}
				set_time_limit(20);
			}
			
			$fname = (isset($_POST['fname'])) ? $_POST['fname'] : 'output';
			file_put_contents('../data/'.$fname.'.xml', $output_dom->saveXML());
			$_SESSION['file'] = 'data/'.$fname.'.xml';
			
			$time_end = microtime(true);
			$time = $time_end - $time_start;
			//echo 'time : '.$time;
			
			header('Location: /accueil.html');
			exit;
		?>