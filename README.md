Projet XML APP5 Polytech Paris-Sud 2017

Groupe:
*Nicolas PERRET
*Christelle NGO BUI HUNG
*Anthony SMOCK
*Mohamed SOLIMANI

====================================================

L'�nonc� du projet est � la racine: projetXML.pdf
Je vous recommande d'installer wamp, et de cr�er un virtual host.
Vous pouvez suivre le instructions en cliquant sur ajouter un virtual host sur l'accueil de wamp.
Vous pouvez sinon suivre ces instructions https://goo.gl/ISiy9X.
J'ai mis un exemple de conf dans le dossier conf.
Le code php est � mettre dans le dossier www.
Je vous laisse t�l�chager le fichier xml de 2Go, mais ne le mettez pas sous git, c'est inutile.
J'ai de toute mani�re ajouter un gitignore.

Pour le projet, je pense que nous pouvons nous d�marquer en utilisant des stream pour pouvoir tout parser. Piste Flux ? XMLReader ?
Date limite: mardi 30 mai.